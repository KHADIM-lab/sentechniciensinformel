'use strict';
module.exports = (sequelize, DataTypes) => {
  var Artisan = sequelize.define('Artisan', {
    prenom: DataTypes.STRING,
    nom: DataTypes.STRING,
    sexe: DataTypes.STRING,
    telephone: DataTypes.STRING,
    email: DataTypes.STRING,
    siteInternet: DataTypes.STRING,
    competence: DataTypes.STRING,
    contact: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        models.Artisan.hasMany(models.Message);
      }
    }
  });
  return Artisan;
};