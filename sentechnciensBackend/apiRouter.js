// Imports
var express      = require('express');
var artisansCtrl    = require('./routes/artisansCtrl');
var messagesCtrl = require('./routes/messagesCtrl');
var likesCtrl    = require('./routes/likesCtrl');

// Router
exports.router = (function() {
  var apiRouter = express.Router();

  // Users routes
  apiRouter.route('/artisans/register/').post(artisansCtrl.register);
  apiRouter.route('/artisans/login/').post(artisansCtrl.login);
  apiRouter.route('/artisans/me/').get(artisansCtrl.getUserProfile);
  apiRouter.route('/artisans/me/').put(artisansCtrl.updateUserProfile);

  // Messages routes
  apiRouter.route('/messages/new/').post(messagesCtrl.createMessage);
  apiRouter.route('/messages/').get(messagesCtrl.listMessages);

  // Likes
  apiRouter.route('/messages/:messageId/vote/like').post(likesCtrl.likePost);
  apiRouter.route('/messages/:messageId/vote/dislike').post(likesCtrl.dislikePost);

  return apiRouter;
})();